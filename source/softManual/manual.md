# 软件使用

## 软件开发环境

|   项目   |                         值                          |         备注          |
| :------: | :-------------------------------------------------: | :-------------------: |
| 开发语言 |                         C++                         |      建议>C++11       |
|  编译器  |                     MingW 7.3.0                     |      MSVC也可以       |
| 开发框架 |                       Qt 5.12                       |       >Qt 5即可       |
|   IDE    |                   Qt Creator 4.10                   |         均可          |
| 操作系统 |                       Windows                       | 目前仅在Windows上编译 |
| 第三方库 |     [QCustomplot](https://www.qcustomplot.com/)     |     数据可视化库      |
|          | [QFtp](https://doc.qt.io/archives/qt-4.8/qftp.html) |      <Qt 4 的库       |
|          |     [qtcsv](https://github.com/iamantony/qtcsv)     |    csv文件的读写库    |

## 软件界面

软件界面如图所示，该软件只有这样的一个界面。

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/%E8%BD%AF%E4%BB%B6%E7%95%8C%E9%9D%A2.jpg" style="zoom:75%;" />

### 登录

登录信息主要是用于与FTP文件服务器连接所需要的信息，该FTP服务器使用Windows Server系统搭建。

#### FTP地址

`FTP地址`处有两个选项，下拉菜单栏是默认的FTP地址，点击不同的FTP地址选项会改变文本框里面的具体地址，这个地址是`IPv4`点分地址，目前接收数据的两台服务器没有域名。

#### 用户名

默认是`RADCALNET`

#### 密码

两台默认的FTP的密码是内部存储的，密码不直接显示

## 数据显示

数据显示主要是显示四种仪器数据，ATR、PSR、HIM、嵩山。

![](https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/%E6%95%B0%E6%8D%AE%E6%98%BE%E7%A4%BA.png)

这四种数据其对应的文件如下所示。

| 仪器 |      用途      |                          路径的例子                          |
| :--: | :------------: | :----------------------------------------------------------: |
| ATR  | 计算通道辐亮度 |                      ATR01/20200408.csv                      |
| PSR  |    反演AOD     |                    PSR03/SUN/20210312.csv                    |
| HIM  |     照度计     | 野外光谱仪/嵩山/**L1_RAD**/HIM_SH02/20210203/RADHIM_SH02_20210203_121808_048.csv |
| 嵩山 |     亮度计     | 野外光谱仪/嵩山/**L1_RAD**/SARS_SS01/**GDAuto**/20191027/RADSARS_SS01_GDAuto_20191027_092408_GD027.csv |

要显示数据，需要选中该文件，然后点击数据显示里面对应的仪器数据，注意需要一一对应，否则会出错，选中的文件如下图所示，会出现一条灰色的线。

#### ATR数据

ATR是一台的八通道的地表辐亮度观测仪器，结果为DN值，转化亮度需要经过定标系数。

ATR仪器实物如下：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/atr.png" style="zoom:80%;" />

ATR文件的DN值如下：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/ATR_data_show.jpg" style="zoom:80%;" />

#### PSR数据

PSR仪器用于反演AOD数据，其反演方法可以查看相关文档，DN值用于反演。

PSR仪器如下：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/psr.jpg" style="zoom:80%;" />

PSR文件的数据如下：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/PSR_data_show.jpg" style="zoom:80%;" />

#### HIM数据

HIM为高光谱辐照度仪，结果为照度。

HIM实物图如下：

![](https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/him.png)

HIM数据如下：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/HIM_data_show.jpg" style="zoom:80%;" />

#### 嵩山亮度计

高光谱辐亮度仪器用来测量地表辐亮度，FTP上面的GDAuto数据就已经是辐亮度了。

嵩山亮度计辐亮度值：

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/SARS_data_show.jpg" style="zoom:80%;" />

## 数据计算

### 嵩山数据计算

数据计算主要用于计算反射率，目前只有嵩山高光谱的反射率可以被计算。嵩山辐亮度计的波长范围是350~1600nm，HIM的照度计数据波长范围是400~2400nm，所以计算的结果是取这两个范围之间的交集，即400~1600nm，其过程如下：

1. 下载嵩山辐亮度计数据和嵩山的HIM数据到本地位置，下载完成是两个.csv文件，如下所示

   ![](https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/down_data.jpg)

2. 计算反射率数据，使用计算公式为$\rho=\frac{\pi L}{E}$，计算过程如图所示，首先点击计算->嵩山反射率-计算，然后需要给两个文件的路径，首先选择辐亮度文件（RADSARS），然后选择照度文件（RADHIM），选择完成点击OK即可得到计算结果；如果需要保存文件则需要点击计算->嵩山反射率->保存按钮，选择需要保存文件的路径即可。
   
   <img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/cal-ref-process1.png" style="zoom:40%;" /><img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/cal-ref-process2.png" style="zoom:40%;" />
   
     <img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/cal-ref-process3.png" style="zoom:40%;" /> <img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/softManual/imgs/cal-ref-process4.png" style="zoom:40%;" />

### 敦煌数据计算

敦煌主要是使用ATR和PSR计算通道反射率，也是使用公式$\rho=\frac{\pi L}{E}$，其中L是ATR获取，E是通过AOD代入大气辐射传输模型模拟获得。

> 这部分代码还未完成