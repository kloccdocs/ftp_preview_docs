# 代码说明

## 文件说明

在文件中，我写的主要代码仅在`mainwindow.h`和`mainwindow.cpp`，即`MainWindow`类，`version.h`是用来做软件的一些信息用的，另外的类都是外部库引入的，如`qcustomplot`、`qftp`、`qurlinfo`这种，所以主要关注MainWindow即可。

![](https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/codeInfo/imgs/file.jpg)

## 界面设计

界面设计使用Qt Creator 的Designer设计，即点击项目列表下的.ui文件即可进入，如下所示。

<img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/codeInfo/imgs/ui-design.jpg" style="zoom:60%;" /><img src="https://gitee.com/kloccdocs/ftp_preview_docs/raw/master/source/codeInfo/imgs/ui-design2.jpg" style="zoom:28%;" />

进入之后即可修改设计布局，增减控件和设置控件的信号-槽函数，这部分的开发在一些教程中有详细的说明，这里就不再赘述了。

## FTP连接

### 自定义信号-槽函数

在MainWindow类的构造函数里面有两行connect代码

```C++	
connect(ui->fileList, &QTreeWidget::itemActivated,this, &MainWindow::processItem);
connect(ui->fileList, &QTreeWidget::itemDoubleClicked, this, &MainWindow::deal_item_double_click);
```



FTP主要是

## QCustomplot绘图

## CSV数据读写